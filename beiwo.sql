set names utf8;
drop database if exists beiwo;
create database beiwo charset=utf8;
use beiwo;

-- 用户信息表
create table `beiwo_user`(
    `id`        int PRIMARY KEY  AUTO_INCREMENT COMMENT '用户ID,主键且自增',
    `username`  varchar(30) NOT NULL COMMENT '用户名,唯一', 
    `password`  varchar(32) NOT NULL COMMENT '密码,MD5',
    `nickname`  varchar(30) DEFAULT NULL, 
    `avatar`    varchar(50)  COMMENT '用户头像'
);

-- 更多成功案例
create table `cases`(
    `category_id`		varchar(32) COMMENT '类别ID',
    `projectOrderId`    int auto_increment  PRIMARY KEY COMMENT '订单id',
    `title`             varchar(32)     COMMENT '标题',
    `area`              Decimal(6,2)    COMMENT '面积',
    `brandName`         varchar(32)     COMMENT '品牌',
    `pvCount`           varchar(32)     COMMENT '热度',
    `imageUrl`          varchar(256)    COMMENT '图片',
    `region`            varchar(16)     COMMENT '地区',
    `decorationForm`    varchar(8)      COMMENT '装修形式',
    `room`              varchar(8)      COMMENT '房间',
    `stageName`         varchar(12)     COMMENT '进度'
    
   );

INSERT INTO `cases` (`category_id`,`projectOrderId` ,`title`,`area`,`brandName`,`pvCount`,`imageUrl`,`region`,`decorationForm`,`room`,`stageName`) VALUES
("1",1,"砖角楼南里", 59.6,"精工","5.6w","1.jpg","朝阳和平里","个性化",2,"整体竣工"),
("1",2,"远洋一方林语苑", 62.6,"精工","2.7w","2.jpg","朝阳双桥","个性化",  2,"整体竣工"),
("1",3,"望京新城", 81.5,"精工","2.6w","3.jpg","朝阳望京","个性化",   3, "整体竣工"),
("1",4,"上地西里", 146.62,"精工","2.2w","4.jpg","海淀上地","个性化",  4, "整体竣工"),
("1",5,"靓景明居",107.7,"精工","4102","5.jpg","通州九棵树(家乐福),","整装",2,"整体竣工"),
("1",6,"芍药居4号院", 95.19, "精工","1.9w","6.jpg","朝阳芍药居","个性化",   4,"整体竣工"),
("1",7,"垡头北里",   46.97, "精工","1.8w","7.jpg","朝阳垡头","个性化",  2, "整体竣工"),
("1",8,"石佛营东里105号院", 45.9,  "精工","1.1w","8.jpg","朝阳石佛营","个性化",2, "整体竣工" ),
("1",9,"首创悦榕汇", 81,     "精工","8298","9.jpg","海淀海淀北部新区","个性化",2, "整体竣工"  ),
("1",10,"法源寺西里小区", 72.31,  "精工","2613","10.jpg","西城牛街","整装",   2, "整体竣工"  ),
("1",11,"和光里",       115,    "精工","8198","11.jpg","丰台五里店","个性化", 0,"整体竣工"  ),
("1",12,"幸福家园三期", 68.7,   "精工","4932","12.jpg","东城广渠门","个性化", 2, "整体竣工" ),
("1",13,"南湖东园二区", 57.3,   "精工","3677","13.jpg","朝阳望京","个性化",2,"整体竣工" ),
("1",14,"花家地西里一区", 50,     "精工","2007","14.jpg","朝阳望京","个性化",2, "整体竣工"  ),
("1",15,"安慧北里小区", 64.48,  "精工","2555","15.jpg","朝阳亚运村","整装",  2, "整体竣工" ),
("1",16,"十里堡东里", 62,   "精工","1769","16.jpg","朝阳十里堡","个性化",2, "整体竣工"),
("1",17,"汇园公寓", 101,  "精工","911","17.jpg","朝阳","个性化",3,"整体竣工"  ),
("1",18,"小黄庄",      0,    "精工","861","18.jpg","朝阳安贞","个性化",1,"整体竣工"),
("1",19,"新潮嘉园二期", 80,   "精工","385","19.jpg","通州潞苑","个性化", 2,"整体竣工" ),
("1",20,"弘善家园", 90.1,  "精工","2147","20.jpg","朝阳潘家园","整装",2,"整体竣工"  ),
("1",21,"富力桃园C区", 60.31, "精工","350","21.jpg","海淀西三旗","个性化", 2,"整体竣工"  ),
("1",22,"大城小镇", 75,    "精工","155","22.jpg","大兴旧宫","个性化",2,"整体竣工"),
("1",23,"景和园",      0,     "精工","148","23.jpg","海淀西北旺","个性化",  0,"整体竣工" ),
("1",24,"东花市北里中区", 81.04, "精工","106","24.jpg","东城东花市","个性化",   2,"整体竣工"  ),
("1",25,"中铁华侨城和园",154.26,"精工","1747","25.jpg","大兴","整装",3,"整体竣工"),
("1",26,"国风北京二期",110,"精工","51","26.jpg","朝阳望京","个性化",3,"中期工程"),
("1",27,"龙跃苑东二区",83.88,"精工","53","27.jpg","昌平回龙观","个性化",2,"整体竣工"),
("1",28,"金顶阳光", 80,    "精工","47","28.jpg","石景山苹果园","个性化",  2,"整体竣工"),
("1",29,"双惠小区", 67,    "精工","27","29.jpg","朝阳双桥","个性化", 2,"整体竣工"  ),
("1",30,"马家堡67号院", 61.55, "精工","729","30.jpg","丰台角门","整装",   2,"整体竣工" ),
("1",31,"融尚未来", 97,    "精工","12","31.jpg","昌平",  "个性化", 3,"整体竣工"),
("1",32,"月坛北小街", 59,    "精工","556","32.jpg","西城月坛","整装",  2,"整体竣工"),
("1",33,"角门东里", 57.2,   "精工","523","33.jpg","丰台角门","整装",   2,"整体竣工" ),
("1",34,"清缘里中区", 70.12,  "精工","499","34.jpg","海淀", "整装",    2,"整体竣工"  ),
("1",35,"定慧东里",  61.4,    "精工","425","35.jpg","海淀定慧寺","整装",    2,"整体竣工" ),
("1",36,"保利西山林语", 93.21,  "精工","384","36.jpg","海淀",   "整装",    2,"整体竣工" ),
("1",37,"望都新地", 89.71,  "精工","375","37.jpg","昌平",   "整装",    2,"整体竣工"  ),
("1",38,"世纪村东区", 58.82,   "精工","375","38.jpg","朝阳亚运村小营","整装",    1,"整体竣工"  ),
("1",39,"马家堡路29号院", 67.9,    "精工","360","39.jpg","丰台洋桥","整装",    3,"整体竣工"   ),
("1",40,"东方银座", 80.76,   "精工","346","40.jpg","东城东直门","整装",   1,"整体竣工" ),
("1",41,"晨月园",77.48,"精工","337","41.jpg","海淀", "整装",2,"整体竣工"),
("1",42,"望园东里",54.18,"精工","332","42.jpg","丰台七里庄","整装",  2,"整体竣工"  ),
("1",43,"丰开苑",64.83,"精工","327","43.jpg","丰台马家堡","整装",    2,"整体竣工"),
("1",44,"望河园",121.44,"精工","318","44.jpg","海淀四季青","整装",   2,"整体竣工"),
("1",45,"郦城三区",  57.53,   "精工","318","45.jpg","海淀四季青","整装",    1,"整体竣工" ),
("1",46,"广华新城", 80,      "精工","306","46.jpg","朝阳百子湾","整装",   2,"整体竣工"  ),
("1",47,"望陶园小区", 108.64,  "精工","307","47.jpg","东城永定门","整装",    2,"整体竣工"  ),
("1",48,"郑王坟280号院", 60,      "精工","305","48.jpg","丰台","整装",   1,"整体竣工"),
("1",49,"凯盛家园", 78.64,   "精工","276","49.jpg","海淀海淀北部新区","整装",   2,"整体竣工"),
("1",50,"辛店小区", 72.4,"精工","213","50.jpg","大兴观音寺","整装",   2,"整体竣工");

-- 首页展示部分成功案例
create table `homepage_cases`(
    `projectOrderId`    varchar(32)     COMMENT '订单id',
    `title`             varchar(32)     COMMENT '标题',
    `area`              Decimal(6,2)    COMMENT '面积',
    `brandName`         varchar(32)     COMMENT '品牌',
    `pvCount`           varchar(32)     COMMENT '热度',
    `imageUrl`          varchar(256)    COMMENT '图片',
    `region`            varchar(16)     COMMENT '地区',
    `decorationForm`    varchar(8)      COMMENT '装修形式',
    `room`              varchar(8)      COMMENT '房间',
    `stageName`         varchar(12)     COMMENT '进度'
);

INSERT INTO `homepage_cases` (`projectOrderId`,`title`,`area`,`brandName`,`pvCount`,`imageUrl`,`region`,`decorationForm`,`room`,`stageName`) VALUES ("35197728598892544","砖角楼南里", 59.6,"精工","5.6w","https://image1.ljcdn.com/utopia-file/d2d8e6ab806ae77f16c64208b6d9ab02.jpg","朝阳和平里","个性化",2,"整体竣工"),
("26873405534715904","远洋一方林语苑", 62.6,"精工","2.7w","https://image1.ljcdn.com/utopia-file/9c2cee7557698ba388fe2df580305f10.jpg","朝阳双桥","个性化",  2,"整体竣工"),   
("31506635105992704","望京新城", 81.5,"精工","2.6w","https://image1.ljcdn.com/utopia-file/23c7f72b4b684f653eba4442c9938d7f.jpg","朝阳望京","个性化",   3, "整体竣工"),  
("31533001381593088","上地西里", 146.62,"精工","2.2w","https://image1.ljcdn.com/utopia-file/00eb76f3846db211b8fff07fb93cc0a2.jpg","海淀上地","个性化",  4, "整体竣工");

-- 轮播图
create table `carousel`(
    `image`     varchar(128)
);
INSERT INTO `carousel` (`image`) VALUES ("https://image1.ljcdn.com/utopia-file/29da692eaa5c90d4a73c790765e08630.jpg"),("https://marketing.ke.com/activity/h5?activityId=102931");

-- 首页高质量服务团队
create table `serviceTeam`(
	`title`	varchar(16),
	`subtitle`	varchar(16),
	`imageUrl`	varchar(256)
);
INSERT INTO `serviceTeam` (`title`,`subtitle`,`imageUrl`) VALUES ("直营工长","5重考核 精工认证","https://img.ljcdn.com/home-web-pic/a230b49562fabbfaf458aaf4aec97a23"),("管家","一对一服务 放心托管","https://img.ljcdn.com/home-web-pic/a5481b94f8e19e84a9f2bf7d095c62b0"),("专业设计师","全案设计 跟进施工","https://img.ljcdn.com/home-web-pic/c654626e4c82dbea0c51b74bc4b771e2");

-- 首页直营体验店
create table `shoplist`(
	`storeId`	varchar(8),
	`storeName`	varchar(16),
	`storeAddress`	varchar(32),
	`schema`	varchar(256),
	`imgUrl`	varchar(256)
);
INSERT INTO `shoplist`(`storeId`,`storeName`,`storeAddress`,`schema`,`imgUrl`) VALUES (1,"万链生活体验馆（北四环店）","北京市朝阳区北四环东路108号千鹤家园底商","https://realsee.com/lianjia/aGKl9J8a4vq3Xx8d/Kbjn1w8XjZ7qhDhyh4tyMyKrHbW7Dlka/","https://image1.ljcdn.com/utopia-file/aedeb800b2d3b0bf8479248c2bc94f0b.png"),
(2,"万链生活体验馆（南四环店）","北京市丰台区马家堡东路189号福海棠华苑2-4底商","https://realsee.com/lianjia/BEy832Ka1rQMNnOe/qk0a74LqNmzDIxh6hVt5z51NSr89MBpA/","https://image1.ljcdn.com/utopia-file/5b9d9f970217d96e3d329ff02124f452.png"),
(3,"被窝家装体验店","北京市海淀区西二旗西路领秀新硅谷B区35号","https://realsee.com/lianjia/XJ6NMLkK7no3Rdx8/ZBVXgbW3YkaVtzh1hpt0xxZ1ul8dO0ox/","https://image2.ljcdn.com/utopia-file/p1/1b4e28b58c2850df448b44ad0ef620f8aada18a9-1005-1005");





--订单详情
create table order_form(
  id int primary key auto_increment,
  user int,
  foreign key(user) references beiwo_user(id),
  orderId int,
  foreign key(orderId) references cases(projectOrderId),
  title varchar(32) not null,
  area decimal(6,2) not null,
  style   boolean not null,
  Decoration_dismantle decimal(5,1),
  Decoration_water decimal(5,1) ,
  Decoration_tax decimal(5,1) not null,
  Decoration_basis decimal(5,1) not null,
  Decoration_sum decimal(5,1) not null,
  dataFurniture_custom  decimal(5,1) not null,
  dataFurniture_other  decimal(5,1) not null,
  dataFurniture_device  decimal(5,1) not null,
  dataFurniture_adornment  decimal(5,1) not null,
  dataFurniture_sum  decimal(5,1) not null,
  sum  decimal(5,1) not null,
  value varchar(32) not null,
  sign boolean ,
  payment boolean,
  img varchar(128) not null
);

--用户评论
create table comment(
 id int primary key auto_increment,
 orderId   int ,
 foreign key(orderId) references cases(projectOrderId),
 user   int,
 foreign key(user) references beiwo_user(id),
 name   varchar(30),
 score   int not null,
 style   boolean not null,
 area   decimal(6,2) not null,
 value   varchar(32) not null,
 content   varchar(600) not null
);

